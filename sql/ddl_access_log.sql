CREATE TABLE `access_log` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `ip` varchar(15) NOT NULL,
  `request` varchar(15) NOT NULL,
  `user_agent` varchar(180) NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) 
