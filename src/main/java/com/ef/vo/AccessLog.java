package com.ef.vo;

import java.util.Date;

public class AccessLog {
	private Integer id;
	private Date date;
	private String ip;
	private String request;
	private String status;
	private String userAgent;

	public AccessLog(){

	}

	public AccessLog(Integer id, Date date, String ip, String request, String status, String userAgent) {
		super();
		this.id = id;
		this.date = date;
		this.ip = ip;
		this.request = request;
		this.status = status;
		this.userAgent = userAgent;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	@Override
	public String toString() {
		return "AccessLogEntity [id=" + id + ", date=" + date + ", ip=" + ip + ", request=" + request + ", status="
				+ status + ", userAgent=" + userAgent + "]";
	}



}
