package com.ef.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	public final static String DEFAULT_FORMAT = "yyyy-MM-dd hh:mm:ss.SSS";
	public final static String PARAM_FORMAT = "yyyy-MM-dd.hh:mm:ss";
	
	public static Date convertStringToDate(String date, String format) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		return simpleDateFormat.parse(date);
	}

}
