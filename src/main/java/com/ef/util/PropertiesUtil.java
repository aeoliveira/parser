package com.ef.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {

	public static Properties getProperties() throws IOException {
		Properties props = new Properties();
		try(InputStream resourceStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("database.properties")) {
			props.load(resourceStream);
		}
		return props;
	}
}
