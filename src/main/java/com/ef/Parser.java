package com.ef;


import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ef.service.AccessLogService;
import com.ef.service.AccessLogServiceImpl;
import com.ef.service.ParserService;
import com.ef.service.ParserServiceImpl;
import com.ef.vo.AccessLog;

public class Parser {
	private String pathName;
	private String startDate;
	private String duration;
	private String threshold;
	
	private static final Logger LOG = LogManager.getLogger(Parser.class);
	
	public static void main(String[] args) {
		java.util.Date init = new java.util.Date();
		
		try {
			new Parser().execute(args);
		} catch (IOException e) {
			LOG.error("Error reading file", e);
		} catch (ParseException e) {
			LOG.error("Error parsing", e);
		} catch (ClassNotFoundException e) {
			LOG.error("Error", e);
		} catch (SQLException e) {
			LOG.error("Error executing query", e);
		}
		
		LOG.info("time execution::: " + (new java.util.Date().getTime() - init.getTime())/1000 + " seconds");
	}

	public void execute(String... args) throws IOException, ParseException, ClassNotFoundException, SQLException {
		ParserService parserService = new ParserServiceImpl();
		AccessLogService accessLogService = new AccessLogServiceImpl();
		
		formatParams(args);
		
		List<AccessLog> list = parserService.getListParsed(pathName);
		accessLogService.insert(list);
		
		list = accessLogService.findAccessLogByDateAndDuration(startDate, duration, threshold);
		
		list.forEach(item -> LOG.info(item.getIp()));
	}

	private void formatParams(String... args) {
		pathName = args[0].substring(args[0].indexOf('/'), args[0].length());
		startDate = args[1].substring(args[1].indexOf('=')+1, args[1].length());
		duration = args[2].substring(args[2].indexOf('=')+1, args[2].length());
		threshold = args[3].substring(args[3].indexOf('=')+1, args[3].length());
	}

}
