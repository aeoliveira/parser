package com.ef.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import com.ef.vo.AccessLog;


public interface ParserService {
	List<AccessLog> getListParsed(String pathname) throws IOException, ParseException;
}
