package com.ef.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import com.ef.vo.AccessLog;

public interface AccessLogService {
	void insert(List<AccessLog> list)throws ClassNotFoundException, SQLException ;
	List<AccessLog> findAccessLogByDateAndDuration(String date, String duration, String threshold)throws ClassNotFoundException, SQLException, ParseException;

}
