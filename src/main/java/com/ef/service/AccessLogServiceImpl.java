package com.ef.service;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ef.dao.AccessLogDao;
import com.ef.dao.AccessLogDaoImpl;
import com.ef.util.DateUtil;
import com.ef.vo.AccessLog;

public class AccessLogServiceImpl implements AccessLogService{
	AccessLogDao dao = new AccessLogDaoImpl(); 

	@Override
	public void insert(List<AccessLog> list) throws ClassNotFoundException, SQLException {
		dao.insert(list);
	}

	@Override
	public List<AccessLog> findAccessLogByDateAndDuration(String date, String duration, String threshold)
			throws ClassNotFoundException, SQLException, ParseException {
		
		Date start = DateUtil.convertStringToDate(date, DateUtil.PARAM_FORMAT);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(start);
		
		if(duration.equalsIgnoreCase("daily")) {
			calendar.add(Calendar.DATE, 1);
		}else if(duration.equalsIgnoreCase("hourly")) {
			calendar.add(Calendar.HOUR, 1);
		}
		
		Timestamp startDate = new Timestamp(start.getTime());
		Timestamp endDate = new Timestamp(calendar.getTimeInMillis());
		
		return dao.findAccessLogByDateAndDuration(startDate, endDate, Integer.valueOf(threshold));
	}

}
