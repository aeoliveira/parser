package com.ef.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.ef.util.DateUtil;
import com.ef.vo.AccessLog;

public class ParserServiceImpl implements ParserService{
	List<AccessLog> list = new ArrayList<>();
	AccessLog entity = null;

	public List<AccessLog> getListParsed(String pathname) throws IOException, ParseException {
		readFile(pathname);
		return list;
	}

	private void readFile(String pathname) throws IOException, ParseException {
		try(BufferedReader in = new BufferedReader(new FileReader(pathname))) {
			String line;
			while ((line = in.readLine()) != null) {
				splitString(line);
			}
		}
	}

	private void splitString(String line) throws ParseException {
		String array[] = line.split(Pattern.quote("|"));
		
		entity = new AccessLog(null, DateUtil.convertStringToDate(array[0], DateUtil.DEFAULT_FORMAT)
				,array[1], array[2], array[3], array[4]);
		
		list.add(entity);
	}

}
