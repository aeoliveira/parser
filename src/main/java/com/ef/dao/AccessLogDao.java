package com.ef.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import com.ef.vo.AccessLog;

public interface AccessLogDao {
	void insert(List<AccessLog> list)throws ClassNotFoundException, SQLException ;
	List<AccessLog> findAccessLogByDateAndDuration(Timestamp startDate, Timestamp endDate, int threshold)throws ClassNotFoundException, SQLException ;

}
