package com.ef.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ef.util.PropertiesUtil;

public class ConnectionFactory {
	
	private static final Logger LOG = LogManager.getLogger(ConnectionFactory.class);

	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		String user = null;
		String password = null;
		String url = null;
		
		try {
			user = PropertiesUtil.getProperties().getProperty("user");
			password = PropertiesUtil.getProperties().getProperty("password");
			url = PropertiesUtil.getProperties().getProperty("url");

		} catch (IOException e) {
			LOG.error(e);
		}
		
		return DriverManager.getConnection(url, user, password);
	}
}
