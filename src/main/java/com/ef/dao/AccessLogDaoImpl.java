package com.ef.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.ef.vo.AccessLog;

public class AccessLogDaoImpl implements AccessLogDao{

	@Override
	public void insert(List<AccessLog> list) throws ClassNotFoundException, SQLException {
		try(Connection connection = ConnectionFactory.getConnection()){

			String sql = "INSERT INTO access_log " +
					"(date, request,user_agent, status, ip) " +
					"VALUES (?,?,?,?,?)";

			PreparedStatement stmt = connection.prepareStatement(sql);

			for (AccessLog accessLog : list) {
				Timestamp date = new Timestamp(accessLog.getDate().getTime());
				int i = 1;

				stmt.clearParameters();
				stmt.setTimestamp(i++, date);
				stmt.setString(i++, accessLog.getRequest().replaceAll("\"", ""));
				stmt.setString(i++, accessLog.getUserAgent());
				stmt.setString(i++, accessLog.getStatus());
				stmt.setString(i++, accessLog.getIp());
				stmt.addBatch();

			}

			stmt.executeBatch();
		}
	}

	@Override
	public List<AccessLog> findAccessLogByDateAndDuration(Timestamp startDate, Timestamp endDate, int threshold)
			throws ClassNotFoundException, SQLException {

		List<AccessLog> list = new ArrayList<>();
		
		try(Connection connection = ConnectionFactory.getConnection()){
			StringBuilder sql = new StringBuilder("SELECT ip, SUM(1) ");
			sql.append("FROM access_log ");
			sql.append("WHERE date BETWEEN ? AND ? ");
			sql.append("GROUP BY ip ");
			sql.append("HAVING SUM(1) > ?; ");
			
			PreparedStatement stmt = connection.prepareStatement(sql.toString());
			stmt.setTimestamp(1, startDate);
			stmt.setTimestamp(2, endDate);
			stmt.setInt(3, threshold);
			
			ResultSet rs = stmt.executeQuery();
			AccessLog accessLog = null;
			
			while (rs.next()) {
				accessLog = new AccessLog();
				accessLog.setIp(rs.getString("ip"));
				
				list.add(accessLog);
			}
		}

		return list;
	}

}
